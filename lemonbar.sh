#!/bin/bash

#RAM() {
	#ARR=$(free -m |sed -n '2p' | cut -c 5-)
	#RAM=$(expr '${ARR[1]}' + '${ARR[4]}' - '${ARR[3]}' - '${ARR[5]}' - '${ARR[6]}')
	#echo -n "$RAM"
#}

Clock(){
	TIME=$(date "+%I:%M")
	echo -e -n "${TIME}"
}

ActiveWindow(){
	WNAME=$(xdotool getwindowfocus getwindowname)
	if [ "$WNAME" = "i3" ]
	then
		echo -n ""
	else
		if [ ${#WNAME} -lt 180 ]
		then
			SPACE=""
			if [ $(((180 - ${#WNAME}) % 2)) -eq 0 ]
			then
				for i in $(seq 1 $(echo "(180-${#WNAME})/2" |bc -l))
				do
					SPACE="$SPACE "
					done
				echo -n "%{B-}$SPACE  $WNAME  $SPACE"
			else
				for i in $(seq 1 $(echo "(179-${#WNAME})/2" |bc -l))
				do
					SPACE="$SPACE "
				done
				echo -n "%{B-}$SPACE  $WNAME $SPACE  "
			fi
		else
			echo -n "%{B-}  $(echo "$WNAME" | cut -c -177)"..."  "
		fi
	fi
}

Workspace(){
	#echo -e  $(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)
	WORKSPACE=$(i3-msg -t get_workspaces | jq '.[] | select(.focused==true).name' | cut -d"\"" -f2)
	if [ $WORKSPACE = 1 ]
	then
		echo -e "I: www"
	elif [ $WORKSPACE = 2 ]
	then
		echo -e "II: term"
	elif [ $WORKSPACE = 3 ]
	then
		echo -e "III: code"
	fi
}

Keycaps(){
	if [ $(xset q | grep Caps |cut -b 22-23) == of ]
	then
		echo -n ""
	else
		echo -n "Keycaps : on"
	fi
}
while true; do
	echo -e  "%{B-}$(Keycaps)%{B#00000000}" "%{c}$(ActiveWindow)%{B#00000000}" "%{r}%{B-}   $(Clock)   %{B#00000000}"
	sleep 0.1s
done
