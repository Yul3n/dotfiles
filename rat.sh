#!/usr/bin/env bash

./lemonbar.sh |lemonbar -p -F#bbc2cf -B#282C34 -f "scientifica"-12 -g 1904x28+8+8 &
ratpoison -c "bind exclam exec rofi -show drun" &
ratpoison -c "escape Super_L" &
xcompmgr &
nitrogen --restore
